
typedef struct Alumno {
	int id;
	char nombre[15];
	char apellido1[20];
	char apellido2[20];
	char email[25];
}Alumno;

typedef struct Curso {
	int id;
	char nombre[30];
}Curso;

typedef struct Asignatura {
	int id;
	int idCurso;
	char nombre[30];
}Asignatura;

typedef struct Nota {
	int idAlumno;
	int idAsignatura;
	int nota;	
}Nota;

void cargarDatos();
void pedirNotasAlumno(int id);
void pedirNotasAlumnos();
void mediaAsignatura(int i);
void mediaAsignaturas();
void notaMediaCurso(int i);
void notaMediaCursos();


