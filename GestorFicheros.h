#include <stdio.h>

void abrirFichero(char *file);
void cerrarFichero();
void cargarFichero(char *file, Alumno alumnos[], Curso cursos[], Asignatura asignaturas[], Nota notas[]);
void cargarAlumnos(Alumno alumnos[]);
void cargarCursos(Curso cursos[]);
void cargarAsignaturas(Asignatura asignaturas[]);
void cargarNotas(Nota notas[]);
void leerAlumno(Alumno alumnos[], int i);
void leerCurso(Curso cursos[], int i);
void leerAsignatura(Asignatura asignaturas[], int i);
void leerNota(Nota notas[], int i);