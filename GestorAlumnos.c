#include "GestorAlumnos.h"
#include "GestorFicheros.h"

static Alumno alumnos[4];
static Curso cursos[2];
static Asignatura asignaturas[10];
static Nota notas[40];

void cargarDatos() {
	cargarFichero("datos.txt", alumnos, cursos, asignaturas, notas);
}

void pedirNotasAlumno(int id) {
	for(int i = 0; i < 4; i++)
		if (alumnos[i].id == id) {
			printf("Nombre: %s\nApellidos: %s %s\n\n", alumnos[i].nombre, alumnos[i].apellido1, alumnos[i].apellido2);
		}
	for (int i = 0; i < 40; i++) {
		if (notas[i].idAlumno == id) {
			printf("%s: %d\n", asignaturas[notas[i].idAsignatura].nombre, notas[i].nota);
		}
	}

	for (int i = 0; i < 40; i++) {
		if (notas[i].idAlumno == id) {
		}
	}
}

void pedirNotasAlumnos() {
	for (int i = 1; i <= 4; i++)
		pedirNotasAlumno(i);
}

void mediaAsignatura(int i) {
	float media = 0;
	float num = 0;
	for (int j = 0; j < 40; j++) {
		if (notas[j].idAsignatura == i) {
			media = media + notas[j].nota;
			num++;
		}
	}
	
	int x = 0;
	while (asignaturas[x].id != i) {
		x++;
	}
	printf("%s: %.2f\n", asignaturas[x].nombre, media/num);
}

void mediaAsignaturas() {
	for (int i = 1; i <= 10; i++)
		mediaAsignatura(i);
}

void notaMediaCursos() {
	float media1 = 0, media2 = 0;
	float num1 = 0, num2 = 0;

	for (int j = 0; j < 40; j++) {
		for (int x = 0; x < 10; x++) {
			if (asignaturas[x].id == notas[j].idAsignatura) {
				if (asignaturas[x].idCurso == 1) {
					media1 = media1 + notas[j].nota;
					num1++;
				}
				else {
					media2 = media2 + notas[j].nota;
					num2++;
				}
			}
		}
	}
	printf("%s: %.2f\n", cursos[0].nombre, media1 / num1);
	printf("%s: %.2f\n", cursos[1].nombre, media2 / num2);
}

		
