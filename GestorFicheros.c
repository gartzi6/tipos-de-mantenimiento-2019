#include <stdio.h>
#include "GestorAlumnos.h"
#include "GestorFicheros.h"

static FILE *fichero = NULL;
errno_t err;

void abrirFichero(char *file) {
	err = fopen_s(&fichero, file, "r");
	if (err == 0)
	{
		printf("Fichero de datos cargado\n");
	}
	else
	{
		printf("No se pudo cargar el fichero\n");
	}
}

void cerrarFichero() {
	if (fichero)
	{
		err = fclose(fichero);
		if (err == 0)
		{
			printf("Fichero cerrado\n");
		}
		else
		{
			printf("No se puedo cerrar el fichero\n");
		}
	}
}

void cargarFichero(char *file, Alumno alumnos[], Curso cursos[], Asignatura asignaturas[], Nota notas[]) {
	abrirFichero(file);
	char basura[4];

	cargarAlumnos(alumnos);
	cargarCursos(cursos);
	cargarAsignaturas(asignaturas);
	cargarNotas(notas);

	printf("Datos cardados\n");

	cerrarFichero();
	
	
}

void cargarAlumnos(Alumno alumnos[]) {
	char c;
	while ((c = fgetc(fichero)) != '\n');

	for (int i = 0; i < 4; i++) {
		leerAlumno(alumnos, i);
	}
	while ((c = fgetc(fichero)) != '\n');
	while ((c = fgetc(fichero)) != '\n');

}

void cargarCursos(Curso cursos[]) {
	char c;
	while ((c = fgetc(fichero)) != '\n');
	for (int i = 0; i < 2; i++) {
		leerCurso(cursos, i);
	}
	while ((c = fgetc(fichero)) != '\n');
	while ((c = fgetc(fichero)) != '\n');
}

void cargarAsignaturas(Asignatura asignaturas[]) {
	char c;
	while ((c = fgetc(fichero)) != '\n');
	for (int i = 0; i < 10; i++) {
		leerAsignatura(asignaturas, i);
	}
	while ((c = fgetc(fichero)) != '\n');
	while ((c = fgetc(fichero)) != '\n');
}

void cargarNotas(Nota notas[]) {
	char c;
	while ((c = fgetc(fichero)) != '\n');
	for (int i = 0; i < 40; i++) {
		leerNota(notas, i);
	}
}

void leerAlumno(Alumno alumnos[], int i) {
	fscanf_s(fichero, "%d", &alumnos[i].id);
	fgetc(fichero);
	fgets(alumnos[i].nombre, 8, fichero);
	fgetc(fichero);
	fgets(alumnos[i].apellido1, 11, fichero);
	fgetc(fichero);
	fgets(alumnos[i].apellido2, 9, fichero);
	fgetc(fichero);
	fgets(alumnos[i].email, 24, fichero);
}
void leerCurso(Curso cursos[], int i){
	fscanf_s(fichero, "%d", &cursos[i].id);
	fgetc(fichero);
	fgets(cursos[i].nombre, 21, fichero);
}
void leerAsignatura(Asignatura asignaturas[], int i){
	fscanf_s(fichero, "%d", &asignaturas[i].id);
	fgetc(fichero);
	fscanf_s(fichero, "%d", &asignaturas[i].idCurso);
	fgetc(fichero);
	fgets(asignaturas[i].nombre, 18, fichero);
}
void leerNota(Nota notas[], int i){
	fscanf_s(fichero, "%d", &notas[i].idAlumno);
	fgetc(fichero);
	fscanf_s(fichero, "%d", &notas[i].idAsignatura);
	fgetc(fichero);
	fscanf_s(fichero, "%d", &notas[i].nota);
}
