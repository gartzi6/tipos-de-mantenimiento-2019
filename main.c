// Tipos de Mantenimiento.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <stdlib.h>
#include <stdio.h>
#include "GestorAlumnos.h"

void mostrarMenu();

int main()
{
	int opt = 0;
	int al = 0;
	cargarDatos();

	while (opt != 6) {
		mostrarMenu();
		
		scanf_s("%d", &opt);
		if (opt != 6) {
			switch (opt) {
			case 1: 
				printf("Las notas de que alumno quieres ver?\n");
				scanf_s("%d", &al);
				pedirNotasAlumno(al);
				break;
			case 2: pedirNotasAlumnos();
				break;
			case 3: 
				printf("De que asignatura quieres saber la media?\n");
				scanf_s("%d", &al);
				mediaAsignatura(al);
				break;
			case 4: mediaAsignaturas();
				break;
			case 5: notaMediaCursos();
				break;
			default: 
				break;
			}
		}
	}

	return 0;
}

void mostrarMenu() {
	printf("Elige una opcion:\n\n");
	printf("1. Ver notas de un alumno\n");
	printf("2. Ver notas de todos los alumnos\n");
	printf("3. Ver notas de una asignatura\n");
	printf("4. Ver notas de todas las asignaturas\n");
	printf("5. Ver nota media de los cursos\n");
	printf("6. Salir\n");
	printf("\nOpcion: ");
}



